#!/usr/bin/python2.7

import time
import sqlobject
from sqlobject import *
import MySQLdb.converters
import sys, os

import gi

gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst

GObject.threads_init()
Gst.init(None)

# uri = sourceServer + vdrId
#
# destination = ''
# channel = 0
#
# videoBitrate = 1000000
# audioBitrate = 65536

videoBitrate0 =   280000
videoKeyFrameInterval0 = 10
videoBitrate1 =   220000
videoKeyFrameInterval1 = 250
videoBitrate2 =  1100000
videoKeyFrameInterval2 = 250
audioBitrate =    65536

# in ms
encoderStartTime = int(round(time.time() * 1000))

lastSegmentEndTime = 0


def _mysql_timestamp_converter(raw):
    """Convert a MySQL TIMESTAMP to a floating point number representing
    the seconds since the Un*x Epoch. It uses custom code the input seems
    to be the new (MySQL 4.1+) timestamp format, otherwise code from the
    MySQLdb module is used."""
    if raw[4] == '-':
        return time.mktime(time.strptime(raw, '%Y-%m-%d %H:%M:%S'))
    else:
        return MySQLdb.converters.mysql_timestamp_converter(raw)


conversions = MySQLdb.converters.conversions.copy()
conversions[MySQLdb.constants.FIELD_TYPE.TIMESTAMP] = _mysql_timestamp_converter

MySQLConnection = sqlobject.mysql.builder()
connection = MySQLConnection(host='intel0', user='pikacu', password='pikacu2000', db='streamer', conv=conversions,
                             debug='false')
sqlhub.processConnection = connection

class Channel(SQLObject):
    vdrId = StringCol()
    name = StringCol()


class FileSegment(SQLObject):
    fileId = BigIntCol()
    startTime = BigIntCol()
    duration = IntCol()
    channel = ForeignKey('Channel')
    firstLastIndex = DatabaseIndex('fileId', 'channel')


class OutPut(Gst.Bin):
    def __init__(self, startFileIndex, outputDirectory):
        super(Gst.Bin, self).__init__()

        self.mux = Gst.ElementFactory.make('mpegtsmux', None)
        self.qOut = Gst.ElementFactory.make('queue2', None)
        self.sink = Gst.ElementFactory.make('multifilesink', None)

        self.add(self.mux)
        self.add(self.qOut)
        self.add(self.sink)

        self.mux.set_property('pat-interval', 900000)
        self.mux.set_property('pmt-interval', 900000)
        self.mux.set_property('dump-until-first-video', 'true')
        self.mux.link(self.qOut)

        self.sink.set_property('location', outputDirectory)
        self.sink.set_property('sync', 'false')
        self.sink.set_property('next-file', 'key-frame')
        self.sink.set_property('post-messages', 'true')
        self.sink.set_property('index', startFileIndex)

        self.qOut.link(self.sink)

class AudioEncoder(Gst.Bin):
    def __init__(self, bitrate):
        super(Gst.Bin, self).__init__()

        self.encoder = Gst.ElementFactory.make('avenc_libfdk_aac', None)
        self.encoder.set_property('bitrate', bitrate)  # tools bitrate

        self.aacparse = Gst.ElementFactory.make('aacparse', None)
        self.aacparse.set_property('disable-passthrough', 'true')

        # self.qOut = Gst.ElementFactory.make('queue2', None)

        # self.audioEncoderTee = Gst.ElementFactory.make('tee', None)

        self.add(self.encoder)
        self.add(self.aacparse)
        # self.add(self.qOut)

        self.encoder.link(self.aacparse)

        # self.aacparse.link_filtered(self.qOut,
        #                          Gst.caps_from_string('audio/mpeg, stream-format=adts')
        # )
        # self.aacparse.link(self.qOut)

        # Add Ghost Pads
        self.add_pad(
            Gst.GhostPad.new('sink', self.encoder.get_static_pad('sink'))
        )

        self.add_pad(
            Gst.GhostPad.new('src', self.aacparse.get_static_pad('src'))
        )

        # self.add_pad(
        #     Gst.GhostPad.new('src', self.qOut.get_static_pad('src'))
        # )


class VideoEncoder(Gst.Bin):
    def __init__(self, bitrate, keyFrameInterval):
        super(Gst.Bin, self).__init__()

        # Create elements
        encoder = Gst.ElementFactory.make('omxh264enc', None)
        h264parser = Gst.ElementFactory.make('h264parse', None)
        qOut = Gst.ElementFactory.make('queue2', None)

        # Add elements to Bin
        self.add(encoder)
        self.add(h264parser)
        self.add(qOut)

        # Set tools properties
        encoder.set_property('target-bitrate', bitrate)  # video tools bitrate
        encoder.set_property('control-rate', 1)  # Variable bitrate
        encoder.set_property('inline-header', 'true')  #
        encoder.set_property('periodicty-idr', keyFrameInterval)
        encoder.set_property('interval-intraframes', keyFrameInterval)  #

        encoder.link_filtered(h264parser,
                              Gst.caps_from_string('video/x-h264, profile=high')
        )

        # parser
        # h264parser.set_property('keep-sps-pps', 'true')
        # h264parser.set_property('fix-sps', 'true')
        # h264parser.set_property('config-interval', 1)
        h264parser.set_property('disable-passthrough', 'true')
        h264parser.set_property('inline-au-delimiter', 'true')  #
        # h264parser.set_property('use-au-delimiter', 'true')

        h264parser.link_filtered(qOut,
                                 Gst.caps_from_string('video/x-h264, alignment=au, stream-format=byte-stream')
        )
        # h264parser.link(qOut)

        # Add Ghost Pads
        self.add_pad(
            Gst.GhostPad.new('sink', encoder.get_static_pad('sink'))
        )
        self.add_pad(
            Gst.GhostPad.new('src', qOut.get_static_pad('src'))
        )

        # Gst.debug_bin_to_dot_file_with_ts( self, Gst.DebugGraphDetails.ALL, "/home/pi/source_encoder5/tools/videoenc.dot")


class EncoderPipeline:
    def __init__(self, sourceServer, channelVdrId):

        self.uri = sourceServer + channelVdrId

        (startfileindex, currentchannelid) = self.getchannelinfo(channelVdrId)
        self.startFileIndex = startfileindex

        # destination location
        self.destination2 = self.formatdestination(currentchannelid, '100')
        self.destination1 = self.formatdestination(currentchannelid, '030')
        self.destination0 = self.formatdestination(currentchannelid, '005')

        # create destination directory
        self.destinationdirectory2 = self.formatdestinationdir(currentchannelid, '100')
        self.destinationdirectory1 = self.formatdestinationdir(currentchannelid, '030')
        self.destinationdirectory0 = self.formatdestinationdir(currentchannelid, '005')
        try:
            os.makedirs(self.destinationdirectory0)
        except OSError:
            print self.destinationdirectory0 + ' exist'
            pass
        try:
            os.makedirs(self.destinationdirectory1)
        except OSError:
            print self.destinationdirectory1 + ' exist'
            pass
        try:
            os.makedirs(self.destinationdirectory2)
        except OSError:
            print self.destinationdirectory2 + ' exist'
            pass


        # print destination

        # clean table records
        # FileSegment.dropTable()
        # FileSegment.createTable()
        #


        # return

        self.mainloop = GObject.MainLoop()
        self.pipeline = Gst.Pipeline()
        self.bus = self.pipeline.get_bus()

        self.bus.add_signal_watch()
        self.bus.enable_sync_message_emission()

        self.bus.connect("sync-message::element", self.on_sync_message)
        self.bus.connect("message", self.on_message)

        self.bus.connect('message::eos', self.on_eos)
        self.bus.connect('message::error', self.on_error)
        #         self.bus.connect('message::eos', self.on_eos)

        # Create elements
        self.player = Gst.ElementFactory.make('uridecodebin', None)

        self.player.set_property('uri', self.uri)
        self.player.set_property('use-buffering', 'false')

        #self.inputVideoRate = Gst.ElementFactory.make('videorate', None)
        #self.inputAudioRate = Gst.ElementFactory.make('audiorate', None)

        self.videoDecTee = Gst.ElementFactory.make('tee', None)
        self.audioDecTee = Gst.ElementFactory.make('tee', None)

        self.encAudioTee = Gst.ElementFactory.make('tee', None)

        self.audioEncoder = AudioEncoder(audioBitrate)

        self.videoRate0 = Gst.ElementFactory.make('videorate', None)
        self.videoScale0 = Gst.ElementFactory.make('videoscale', None)
        self.videoScale0.set_property('method', 1)
        self.videoEncoder0 = VideoEncoder(videoBitrate0, videoKeyFrameInterval0)
        self.outPut0 = OutPut(startfileindex, self.destination0)

        self.videoScale1 = Gst.ElementFactory.make('videoscale', None)
        self.videoScale1.set_property('method', 0)
        self.videoEncoder1 = VideoEncoder(videoBitrate1, videoKeyFrameInterval1)
        self.outPut1 = OutPut(startfileindex, self.destination1)
        self.audioQueue1 = Gst.ElementFactory.make('queue2', None)

        self.videoEncoder2 = VideoEncoder(videoBitrate2, videoKeyFrameInterval2)
        self.outPut2 = OutPut(startfileindex, self.destination2)
        self.audioQueue2 = Gst.ElementFactory.make('queue2', None)

        # Add elements to pipeline      
        self.pipeline.add(self.player)
        self.pipeline.add(self.audioEncoder)

        #self.pipeline.add(self.inputAudioRate)
        #self.pipeline.add(self.inputVideoRate)
        self.pipeline.add(self.videoDecTee)
        self.pipeline.add(self.audioDecTee)
        self.pipeline.add(self.encAudioTee)


        self.pipeline.add(self.videoRate0)
        self.pipeline.add(self.videoScale0)
        self.pipeline.add(self.videoEncoder0)
        self.pipeline.add(self.outPut0)

        self.pipeline.add(self.videoScale1)
        self.pipeline.add(self.videoEncoder1)
        self.pipeline.add(self.audioQueue1)
        self.pipeline.add(self.outPut1)

        self.pipeline.add(self.videoEncoder2)
        self.pipeline.add(self.audioQueue2)
        self.pipeline.add(self.outPut2)




        self.audioLinked = False;

        # Connect signal handlers
        self.player.connect('pad-added', self.on_pad_added)

        #self.inputVideoRate.link_filtered(self.videoDecTee,
        #                          Gst.caps_from_string('video/x-raw, framerate=25/1')
        #)

        # quality 0
        self.videoDecTee.link(self.videoRate0)
        self.videoRate0.link_filtered(self.videoScale0,
                                 Gst.caps_from_string('video/x-raw, framerate=1/1')
        )
        self.videoScale0.link_filtered(self.videoEncoder0,
                                 Gst.caps_from_string('video/x-raw, width=360, height=288')
        )
        self.videoEncoder0.link(self.outPut0.mux)

        # quality 1
        self.videoDecTee.link(self.videoScale1)
        self.videoScale1.link_filtered(self.videoEncoder1,
                                 Gst.caps_from_string('video/x-raw, width=360, height=288')
        )

        self.videoEncoder1.link(self.outPut1.mux)

        # quality 2
        self.videoDecTee.link(self.videoEncoder2)
        self.videoEncoder2.link(self.outPut2.mux)

        # audio tools
        self.audioDecTee.link(self.audioEncoder)
	
	# audiorate
        #self.audioDecTee.link(self.inputAudioRate)
        #self.inputAudioRate.link_filtered(self.audioEncoder,
        #                      Gst.caps_from_string('audio/x-raw, format=S16LE, rate=48000')
        #)
        #self.inputAudioRate.link(self.audioEncoder)

        self.audioEncoder.link_filtered(self.encAudioTee,
                                 Gst.caps_from_string('audio/mpeg, stream-format=adts')
        )
        # self.audioEncoder.link_filtered(self.encAudioTee,
        #                          Gst.caps_from_string('audio/mpeg, stream-format=raw')
        # )

        #
        # audio for quality 1,2
        self.encAudioTee.link(self.audioQueue1);
        self.audioQueue1.link(self.outPut1.mux)
        self.encAudioTee.link(self.audioQueue2);
        self.audioQueue2.link(self.outPut2.mux)


    def formatdestination(self, selectedChannelId, qualityPrefix):
        return '/media/live/' + str(selectedChannelId) + '/' + qualityPrefix + '/' + '%010d.ts'

    def formatdestinationdir(self, selectedChannelId, qualityPrefix):
        return '/media/live/' + str(selectedChannelId) + '/' + qualityPrefix + '/'

    def getchannelinfo(self, channelVdrId):
        startFileIndex = 0
        currentChannel = Channel.select(Channel.q.vdrId == channelVdrId)
        self.currentChannel = (list(currentChannel))[0]
        print self.currentChannel

        #get last file index from db
        #res = FileSegment.select("file_segment.id = (SELECT MAX( file_segment.id ) FROM file_segment WHERE channel_id= )" % )
        res = FileSegment.select(
            "file_segment.id = (SELECT MAX( file_segment.id ) FROM file_segment WHERE channel_id=%s )" % self.currentChannel.id)
        #res = FileSegment.select("channel_id=%s" % 20 )
        result = list(res)

        if (len(result) > 0):
            print('last file in table:', result[0].fileId)
            startFileIndex = result[0].fileId + 1

        print startFileIndex

        return (startFileIndex, self.currentChannel.id)

    def on_sync_message(self, bus, msg):
        if (msg.src == self.outPut2.sink ):
            print('on_sync_message:', msg, msg.type, msg.src)

    def on_message(self, bus, msg):
        # print('on_message:' , msg, msg.type, msg.src)
        if (msg.src == self.outPut2.sink):
            #             print('on_message:' , msg, msg.type, msg.src)
            #             if (msg.type == Gst.MessageType.TAG):
            #                 print(Gst.TagList.n_tags(msg.parse_tag()))
            #                 tagList = msg.parse_tag()
            #                 count = Gst.TagList.n_tags(tagList)
            #                 for i in range(count) :
            #                     print Gst.TagList.nth_tag_name(tagList, i)
            #             el
            if (msg.type == Gst.MessageType.ELEMENT):
                #                print('on_message:' , msg, msg.type, msg.src)
                st = msg.get_structure();
                #                print(st.get_name())
                count = st.n_fields()
                #                 print(count)
                #                print("filename", st.get_value("filename"))
                #                print("index", st.get_value("index"))
                #                print("running-time", st.get_value("running-time"))
                #                print("stream-time", st.get_value("stream-time"))
                #                print("duration", st.get_value("duration"))
                #                print("segment-duration", st.get_value("segment-duration"))

                currentSegmentIndex = st.get_value("index")
                currentSegmentEndTime = st.get_value("stream-time")

                global lastSegmentEndTime

                # in ms
                segmentDuration = (currentSegmentEndTime - lastSegmentEndTime) / 1000000

                # in ms
                segmentStartTime = encoderStartTime + (lastSegmentEndTime / 1000000)

                print("[" + str(currentSegmentIndex) + "]time:" + str(time.gmtime(segmentStartTime / 1000)) + " segmentStartTime:" + str(segmentStartTime))

                newFile = FileSegment(fileId=currentSegmentIndex, startTime=segmentStartTime, channel=self.currentChannel,
                                      duration=segmentDuration)

                lastSegmentEndTime = currentSegmentEndTime

            #                 for i in range(count) :
            #                      print (st.nth_field_name(i))
            #                 print(key)
            #             return
            #         if(msg.src == self.sink ):
            #         print(msg.structure)
            #         if msg.type == Gst.MessageType.TAG:
            #             print("tag message:", Gst.TagList.n_tags(msg.parse_tag()))

            #             msg.parse_tag().
            #             Gst.taglistntags(msg.parse_tag())
            #             for key in msg.parse_tag():
            #                 print(key)
            #             return

            #         print(Gst.Message.GST_MESSAGE_TAG);
            #         print(Gst.MESSAGETAG)

    def run(self):
        self.pipeline.set_state(Gst.State.PLAYING)
        self.mainloop.run()

    def kill(self):
        self.pipeline.set_state(Gst.State.NULL)
        self.mainloop.quit()

    def on_pad_added(self, element, pad):
        string = pad.query_caps(None).to_string()
        print('on_pad_added():', string)
        print(self.pipeline)
        # Gst.debug_bin_to_dot_file_with_ts( self.pipeline, Gst.DebugGraphDetails.ALL, "/home/pi/source_encoder5/tools/error.dot")
        if string.startswith(
                'audio/x-raw, format=(string)S16LE, layout=(string)interleaved, rate=(int)48000, channels=(int)2'):
            if self.audioLinked == False:
                pad.link(self.audioDecTee.get_static_pad('sink'))
                #pad.link(self.audioEncoder.get_static_pad('sink'))
                self.audioLinked = True
            else:
                print('audio already linked')
        elif string.startswith(
                'audio/x-raw, format=(string)S16LE, layout=(string)interleaved, rate=(int)48000, channels=(int)1'):
            if self.audioLinked == False:
                pad.link(self.audioDecTee.get_static_pad('sink'))
                # pad.link(self.audioEncoder.get_static_pad('sink'))
                self.audioLinked = True
            else:
                print('audio already linked')
        elif string.startswith('video/'):
            #pad.link(self.inputVideoRate.get_static_pad('sink'))
            pad.link(self.videoDecTee.get_static_pad('sink'))

    def on_eos(self, bus, msg):
        print('on_eos()')
        self.kill()

    def on_error(self, bus, msg):
        # Gst.debug_bin_to_dot_file_with_ts( self.pipeline, Gst.DebugGraphDetails.ALL, "error.dot")
        print('on_error():', msg.parse_error())
        self.kill()


# print(result)

# example = Encoder('http://pi3:3000/', 'T-8395-518-773')
# example.run()

 