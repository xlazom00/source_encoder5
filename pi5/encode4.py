#!/usr/bin/python2.7

import sys

sys.path.append("../")

import tools.encoder26

# server
sourceServer = 'http://intel0:3000/'
#sourceServer = 'http://pi2:3000/'

# NOVA
vdrId = 'T-8395-518-513'

# Prima
#vdrId = 'T-8395-518-773'

# NOVA CINEMA
#vdrId = 'T-8395-518-514'

# COOL
#vdrId = 'T-8395-518-770'

# CT2
#vdrId = 'T-8395-273-258'

# Prima LOVE
#vdrId = 'T-8395-770-772'



enc = tools.encoder26.EncoderPipeline(sourceServer, vdrId)
enc.run()
