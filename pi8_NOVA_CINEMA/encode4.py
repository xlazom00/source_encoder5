#!/usr/bin/python2.7

import sys

sys.path.append("../")

import tools.encoder34_in

# server
#sourceServer = 'http://pi2:3000/'
sourceServer = 'http://intel0:3000/'

# SMICHOV
#vdrId = 'T-8395-1028-517'

# Prima
#vdrId = 'T-8395-518-773'

# Prima LOVE
#vdrId = 'T-8395-770-772'

# NOVA CINEMA
vdrId = 'T-8395-518-514'

# CT2
#vdrId = 'T-8395-273-258'

# COOL
#vdrId = 'T-8395-518-770'


enc = tools.encoder34_in.EncoderPipeline(sourceServer, vdrId)
enc.run()
