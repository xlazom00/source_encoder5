#!/usr/bin/python2.7

import sys

sys.path.append("../")

import tools.encoder27

# server
sourceServer = 'http://intel0:3000/'
#sourceServer = 'http://pi2:3000/'

# CT 1
vdrId = 'T-8395-273-257'

# CT D/ART
#vdrId = 'T-8395-770-264'

# PRIMA LOVE 
#vdrId = 'T-8395-770-772'

# CT 2
#vdrId = 'T-8395-273-258'

# CT SPORT
#vdrId = 'T-8395-273-260'

# NOVA
#vdrId = 'T-8395-518-513'

# Prima
#vdrId = 'T-8395-518-773'

# NOVA CINEMA
#vdrId = 'T-8395-518-514'

# COOL
#vdrId = 'T-8395-518-770'

# CT2
#vdrId = 'T-8395-273-258'

# Prima LOVE
#vdrId = 'T-8395-770-772'

# SMICHOV
#vdrId = 'T-8395-1028-517'

enc = tools.encoder27.EncoderPipeline(sourceServer, vdrId)
enc.run()
