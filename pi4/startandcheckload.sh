#!/bin/bash

APP_TO_RUN=$1
#APP_TO_RUN="./load.sh"

trap 'kill $(jobs -p)' EXIT

APP_TO_RUN_PID=-1


#echo "$APP_TO_RUN"
#exit 0

start_app()
{
	APP_TO_RUN_PID="-1"
	#ORC_CODE=backup GST_DEBUG="*:1" 
	eval "$1 &"
	APP_TO_RUN_PID=$!
	APP_FILENAME=${APP_TO_RUN:2:1000}
	echo "$APP_TO_RUN_PID" > "$APP_FILENAME.pid"
	date >> "$APP_FILENAME.start"
	echo "pid: $APP_TO_RUN_PID"
	return 0
}

function proc_usage()
{
    nPid=$1;
    nTimes=10; # customize it
    delay=0.1; # customize it
    strCalc=`top -d $delay -b -n $nTimes -p $nPid \
    |grep $nPid \
    |sed -r -e "s;\s\s*; ;g" -e "s;^ *;;" \
    |cut -d' ' -f9 \
    |tr '\n' '+' \
    |sed -r -e "s;(.*)[+]$;\1;" -e "s/.*/scale=2;(&)\/$nTimes/"`;
    #echo "$strCalc"
    nPercCpu=`echo "$strCalc" |bc -l`
    if [ -z "$nPercCpu" ];
    then
	echo "0"
    else
	echo "${nPercCpu%.*}"
    fi
}

start_app "$APP_TO_RUN"

#echo "pid: $APP_TO_RUN_PID"

PID_LOAD=100

while [ 1 ] ;
do
	sleep 10s
	# check cpu load
#	echo "measure load"
	CUR_PID_LOAD=$(proc_usage ${APP_TO_RUN_PID})
#	echo "measure load done"

#	CUR_PID_LOAD=`./procusage.sh "$APP_TO_RUN_PID"`

	echo "LOAD:$CUR_PID_LOAD LASTLOAD: $CUR_PID_LOAD"
	if [ -z "$CUR_PID_LOAD" ];
	then
	    echo "no running"
	    kill -n 9 "$APP_TO_RUN_PID"
	    start_app "$APP_TO_RUN"
	elif [ "$CUR_PID_LOAD" -gt 400 ];
	then
	    echo "loadover 400"
	    kill -n 9 "$APP_TO_RUN_PID"
	    start_app "$APP_TO_RUN"
	else
	    PID_LOAD=$(($PID_LOAD + $CUR_PID_LOAD))
	    PID_LOAD=$(($PID_LOAD / 2))
	    #if [ -z "$PID_LOAD" ];
	    echo "PID_LOAD:$PID_LOAD"
	    if [ "$PID_LOAD" -lt 10 ];
	    then
		echo "kill $APP_TO_RUN_PID"
		kill -n 9 "$APP_TO_RUN_PID"
		echo "low load!!!!"
		start_app "$APP_TO_RUN"
	    fi
	fi
done
